% This code was created by Joel Rosenfeld in 2021 to accompany his YouTube
% channel ThatMaththing (http://www.thatmaththing.com/
% If you use this code for a project, please credit Joel A. Rosenfeld, and
% link his YouTube channel and professional website,
% http://www.thelearningdock.org/

% The video in question can be found here: https://youtu.be/TuWqNSbgHj4

% Function To Interpolate

f = @(x) sin(2*pi*x) + (x.^3)/5 + cos(2*pi/5*x);

% Plot Function To Interpolate over [0, 3]

t_evaluate = 0:0.01:3;

figure

plot(t_evaluate,f(t_evaluate),'LineWidth',3);

hold on;

% Sample Sites

t_samples = 0:0.1:3

plot(t_samples,f(t_samples),'o','LineWidth',6);

% Kernel Function

mu = 0.75;

K = @(x,y) exp(-1/mu*norm(x-y)^2);

%mu = 1;
%K = @(x,y) exp(1/mu*x'*y);

% Gram Matrix

GramMatrix = zeros(length(t_samples));

for i = 1:length(t_samples)
    for j = 1:length(t_samples)
        GramMatrix(i,j) = K(t_samples(i),t_samples(j));
    end
end

% Sampled Output
Y = f(t_samples)';

% Weights

Weights = pinv(GramMatrix)*Y;

% Evaluating the Approximation

Z = zeros(1,length(t_evaluate));

for i = 1:length(t_evaluate)
    sum = 0;
    for j = 1:length(t_samples)
        sum = sum + Weights(j)*K(t_evaluate(i),t_samples(j));
    end
    Z(i) = sum;
end

% Plot

figure

plot(t_evaluate,Z,'LineWidth',3);
hold on

plot(t_evaluate,f(t_evaluate),'--','LineWidth',3);
plot(t_samples,f(t_samples),'o','LineWidth',6);
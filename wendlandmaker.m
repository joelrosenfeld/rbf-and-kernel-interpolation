k = 3;
s = 3;

syms r;

basefunction = (1-r)^(floor(s/2) + k + 1);

integrated = int(r*basefunction);

wendlandfunction = subs(integrated,r,1) - integrated;

for i = 2:k
    integrated = int(r*wendlandfunction);
    wendlandfunction = subs(integrated,r,1) - integrated;
end

wendlandfunction = matlabFunction(wendlandfunction/subs(wendlandfunction,r,0));

wendlandfunction = @(t) wendlandfunction(t).*(t < 1);